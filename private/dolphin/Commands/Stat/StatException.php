<?php
/**
 * Wofh Tools
 *
 * @since       19.06.2016 18:20
 * @package     WofhTools\Tools\Statistic
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   Copyright © 2016 delphinpro
 * @license     Licensed under the MIT license
 */

namespace Dolphin\Commands\Stat;


/**
 * Class StatException
 *
 * @package WofhTools\Tools\Statistic
 */
class StatException extends \Exception
{
    const ERROR_CODE_DUPLICATE_DATE = 2;
}
