<?php

namespace WofhTools\Tools;


/**
 * Class WofhException
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   copyright © 2019 delphinpro
 * @license     licensed under the MIT license
 * @package     WofhTools\Tools
 */
class WofhException extends \Exception
{
}
