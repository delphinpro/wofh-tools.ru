<?php

namespace WofhTools\Helpers;


/**
 * Class FileSystemException
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   Copyright © 2019 delphinpro
 * @license     Licensed under the MIT license
 * @package     WofhTools\Helpers
 */
class FileSystemException extends \Exception
{
}
