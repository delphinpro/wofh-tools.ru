<?php

namespace WofhTools\Helpers;


/**
 * Class JsonCustomException
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   copyright © 2019 delphinpro
 * @license     licensed under the MIT license
 * @package     WofhTools\Helpers
 */
class JsonCustomException extends \Exception
{
}
