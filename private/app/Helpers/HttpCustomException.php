<?php

namespace WofhTools\Helpers;


/**
 * Class HttpCustomException
 *
 * @author      delphinpro <delphinpro@gmail.com>
 * @copyright   Copyright © 2019 delphinpro
 * @license     Licensed under the MIT license
 * @package     WofhTools\Helpers
 */
class HttpCustomException extends \Exception
{
}
