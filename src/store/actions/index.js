/**
 * WofhTools. Store root actions.
 *
 * (c) 2019 delphinpro <delphinpro@gmail.com>
 * licensed under the MIT license
 */


export const LOADING_UP = 'LOADING_UP';
export const LOADING_DOWN = 'LOADING_DOWN';
export const ACTIVE_WORLDS = 'ACTIVE_WORLDS';
