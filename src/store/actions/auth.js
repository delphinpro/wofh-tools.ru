/*!
 * WofhTools
 * File: store/actions/auth.js
 * © 2019 delphinpro <delphinpro@gmail.com>
 * licensed under the MIT license
 */

export const AUTH_REQUEST = 'AUTH_REQUEST';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_ERROR = 'AUTH_ERROR';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
