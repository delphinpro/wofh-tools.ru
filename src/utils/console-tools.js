/*!
 * WofhTools
 * (c) 2019 delphinpro <delphinpro@gmail.com>
 * licensed under the MIT license
 */

export const colors = {
    info: 'color:#3B85CE',
    warn: 'background-color:#363005;color:#FDDF9D',
    danger: 'color:#E33730',
};
