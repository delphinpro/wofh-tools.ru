/**
 * WofhTools. Global constants.
 *
 * (c) 2019 delphinpro <delphinpro@gmail.com>
 * licensed under the MIT license
 */

export const LS_KEY_TOKEN = 'JWT';
export const HTTP_HEADER_AUTHORIZATION = 'Authorization';
